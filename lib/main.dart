import 'package:flutter/material.dart';
import 'package:lab12/src/pages/home_temp.dart';
import 'package:lab12/src/pages/option_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      //home: HomePageTemp(),
      routes: {
        '/': (context) => HomePageTemp(),
        'alert': (context) => OptionPage(),
        'avatar': (context) => OptionPage(),
        'card': (context) => OptionPage(),
      },
    );
  }
}
