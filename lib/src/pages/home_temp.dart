import 'package:flutter/material.dart';
import 'package:lab12/src/providers/menu_provider.dart';
import 'package:lab12/src/utils/icono_string_utils.dart';

class HomePageTemp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Componentes'),
        ),
        body: FutureBuilder(
          future: menuProvider.cargarData(),
          initialData: [],
          builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
            return ListView.separated(
              itemCount: snapshot.data.length,
              itemBuilder: (context, i) {
                return _buildRow(snapshot.data[i], context);
              },
              separatorBuilder: (context, i) {
                return Divider();
              },
            );
          },
        ));
  }

  Widget _buildRow(option, context) {
    return ListTile(
      leading: getIcon(option['icon']),
      title: Text(
        option['texto'],
        style: TextStyle(fontWeight: FontWeight.w500),
      ),
      trailing: Icon(
        Icons.navigate_next,
        color: Colors.red,
      ),
      onTap: () {
        print(option['ruta']);
        Navigator.pushNamed(context, option['ruta'],
            arguments: option['texto']);
      },
    );
  }
}
