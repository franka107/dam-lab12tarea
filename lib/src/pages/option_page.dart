import 'package:flutter/material.dart';

class OptionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(args),
      ),
      body: Text('esta es la ventana de $args'),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back),
          tooltip: 'Add',
          onPressed: () {
            Navigator.pop(context);
          }),
    );
  }
}
